<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/invite', 'UsersController@inviteUsers')->name('users.invite');
Route::get('users/removeuser/{user}', 'UsersController@remove')->name('users.remove');
Route::get('users/makeleader/{user}', 'UsersController@makeLeader')->name('users.makeLeader');

Route::resource('/teams', 'TeamsController');
Route::resource('/tasks', 'TasksController');

// Route::middleware(['auth', 'admin'])->group(function(){
//     Route::resource('/teams', 'TeamsController');
// });


