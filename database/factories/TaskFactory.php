<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Task;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'title'=> $faker->sentence(rand(6,12)),
        'description'=> $faker->paragraph(rand(1, 4), true),
        'due_date'=> Carbon::now()->addDay()->format('Y-m-d'),
        'priority'=> rand(0, 2),
        'status'=> 'inprogress',
        'team_id'=> rand(1, 4)
    ];
});
