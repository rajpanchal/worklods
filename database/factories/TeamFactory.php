<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Team;
use Faker\Generator as Faker;

$factory->define(Team::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(rand(1,2)),
        'description' => $faker->sentence(rand(4,8))
    ];
});
