<?php

use App\Task;
use App\Team;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        factory(Team::class, 4)->create()
            ->each(function($team){
                $team->users()
                    ->saveMany(factory(User::class, rand(4, 7))->make())
                    ->each(function($user){
                        $user->tasks()->saveMany(factory(Task::class, rand(1, 4))->make());
                    });
        });

    }
}
