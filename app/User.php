<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'team_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function bestEmpolyeeOfTheMonth() {
        $totalPoints = 0;
        $allUsersTotalPoint = [];
        $users = User::all();
        foreach($users as $user) {
            $points = Points::all()->where('user_id', '=', $user->id);
            // dd($points->first() == null);
            if($points->first() != null){
                foreach($points as $point) {
                    $totalPoints += $point->point;
                }
                array_push($allUsersTotalPoint, [['user' => $user], ['point' => $totalPoints]]);
                // dd($allUsersTotalPoint);
                $totalPoints = 0;
            }
        }
        // dd($allUsersTotalPoint);
        $empOfTheMonth = collect($allUsersTotalPoint)->sortBy($totalPoints);
        // dd($empOfTheMonth->first()[0]['user_id']);
        // $bestEmpOfTheMonth = new BestEmpOfTheMonth;
        // $bestEmpOfTheMonth->user_id = $empOfTheMonth->first()[0]['user_id'];
        // $bestEmpOfTheMonth->date = Carbon::now();
        // $bestEmpOfTheMonth->save();
        // dd($empOfTheMonth->first()[0]['user']->id);
        $bestEmpOfTheMonth = BestEmpOfTheMonth::create([
            'user_id' => $empOfTheMonth->first()[0]['user']->id,
            'date' => Carbon::now(),
        ]);
        return $empOfTheMonth->first()[0]['user']->name;
    }


    /**
     * ACCESSORS
     */

    public function getAvatarAttribute() {
        $size = 36;
        $font_size = "0.4rem";
        $name = $this->name;
        return "https://ui-avatars.com/api/?name={$name}&rounded=true&size={$size}&font-size={$font_size}";
    }

    public function isAdmin(){
        return $this->role === 'admin';
    }

    public function isLeader(){
        return $this->role === 'leader';
    }

    public function isMember(){
        return $this->role === 'member';
    }


    /**
     * Relationship Methods
     *
     */
    public function tasks(){
        return $this->hasMany(Task::class);
    }
    public function teams(){
        return $this->belongsTo(Team::class);
    }
}
