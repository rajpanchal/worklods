<?php

namespace App\Http\Controllers;

use App\Http\Requests\Task\CreateTaskRequest;
use App\Http\Requests\Task\UpdateTaskRequest;
use App\Points;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::all();
        return view('tasks.index', compact([
            'tasks'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $team_id = auth()->user()->team_id;
        $users = DB::table('users')->where('team_id', '=', $team_id)->get();
        return view('tasks.create', compact([
            'users'
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTaskRequest $request)
    {
        // dd($request->team_id);
        $status = "inprogress";
        if($request->user == "-2"){
            $user_id = Task::autoAssign($request->team_id);
        }elseif($request->user == "-1"){
            $user_id = null;
            $status = "open";
        }else{
            $user_id = $request->user;
        }
        $task = Task::create([
            'user_id' => $user_id,
            'team_id' => $request->team_id,
            'title' => $request->title,
            'description' => $request->description,
            'due_date' => $request->due_date,
            'priority' => $request->priority,
            'status' => $status,
        ]);

        session()->flash('success', "Task has been added successfully!");
        return redirect(route('tasks.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        return view('tasks.show', compact([
            'task',
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        $team_id = auth()->user()->team_id;
        $users = DB::table('users')->where('team_id', '=', $team_id)->get();
        return view('tasks.edit', compact([
            'task',
            'users'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTaskRequest $request, Task $task)
    {
        $points = DB::table('points')->where([
            ['user_id', '=', $task->user_id],
            ['task_id', '=', $task->id]
        ])->get()->first();
        if($points == null){
            $oldPoint = 0;
        }else{
            $oldPoint = $points->point;
        }
        if($request->status == "underreview"){
            $point = $oldPoint + $task->priority;
        }elseif($request->status == "quitted"){
            $point = $oldPoint - 2;
        }elseif($request->status == "rejected"){
            $point = $oldPoint - 2;
        }elseif($request->status == "approved"){
            $point = $oldPoint + 1;
        }
        if($points == null){
            $points = new Points;
            $points->user_id = $task->user_id;
            $points->task_id = $task->id;
            $points->point = $point;
            $points->status = $request->status;
            $points->save();
        }else{
            DB::table('points')->where([
                ['user_id', '=', $task->user_id],
                ['task_id', '=', $task->id]
            ])->update([
                'point' => $point,
                'status' => $request->status
            ]);
        }
        $task->update([
            'status'=>$request->status
        ]);
        session()->flash('success', "Task has been updated successfully!");
        return redirect(route('tasks.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
    }
}
