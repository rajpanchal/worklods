<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Http\Request;
use App\Http\Requests\Team\CreateTeamRequest;
use App\Http\Requests\Team\UpdateTeamRequest;
use App\User;
use Illuminate\Support\Facades\DB;

class TeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::all();
        $users = User::all();
        return view('teams.index', compact([
            'teams',
            'users'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = DB::table('users')->where([
            ['team_id', '=', null],
            ['role', '=', 'member'],
        ])->get();

        return view('teams.create', compact([
            'users'
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTeamRequest $request)
    {
        // dd($request);
        Team::create([
            'name'=>$request->name
        ]);
        $team_id = DB::table('teams')->latest('created_at')->first()->id;
        $users = $request->users;
        foreach($users as $user) {
            DB::table('users')->where('id', $user)->update(['team_id' => $team_id]);
        }
        session()->flash('success', "Team has been added successfully!");
        return redirect(route('teams.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        $users = User::all()->where('team_id', '=', $team->id);
        return view('teams.show', compact([
            'team',
            'users'
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        $users = DB::table('users')->where([
            ['team_id', '=', $team->id],
        ])
        ->orWhere([
            ['team_id', '=', null],
        ])
        ->get();
        return view('teams.edit', compact([
            'users',
            'team'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTeamRequest $request, Team $team)
    {
        // dd($request->description);
        $team->update([
            'name'=>$request->name,
            'description'=>$request->description
        ]);
        $oldUsers = User::all()->where('team_id', '=', $team->id);
        foreach($oldUsers as $user) {
            DB::table('users')->where('id', $user)->update(['team_id' => null]);
        }
        $users = $request->users;
        foreach($users as $user) {
            DB::table('users')->where('id', $user)->update(['team_id' => $team->id]);
        }
        session()->flash('success', 'Team Updated Successfully!');
        return redirect(route('teams.show', $team->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
        $team->delete();
        session()->flash('success', "Team has been deleted successfully!");
        return redirect(route('teams.index'));
    }
}
