<?php

namespace App\Http\Controllers;

use App\Team;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    public function inviteUsers(){
        return view('users.invite');
    }

    public function remove(User $user){
        $role = "member";
        $user->update([
            'team_id'=>null,
            'role'=>$role
        ]);
        session()->flash('success', "User has been removed successfully!");
        return redirect()->back();
    }

    public function makeLeader(User $user){
        $leader = User::all()->where('team_id', '=', $user->team_id)->where('role', '=', 'leader')->first();
        if($leader){
            $leader->update([
                'role'=>'member'
            ]);
        }
        $user->update([
            'role'=>'leader'
        ]);
        session()->flash('success', "User has been removed successfully!");
        return redirect()->back();
    }
}
