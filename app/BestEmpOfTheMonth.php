<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BestEmpOfTheMonth extends Model
{
    protected $fillable = [
        'user_id',
        'date',
    ];
}
