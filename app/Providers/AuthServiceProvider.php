<?php

namespace App\Providers;

use App\Policies\TeamsPolicy;
use App\Team;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        Team::class => TeamsPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isAdmin', function ($user) {
            return $user->role === "admin";
        });

        Gate::define('isLeader', function ($user) {
            return $user->role === "leader";
        });

        Gate::define('isMember', function ($user) {
            return $user->role === "member";
        });
    }
}
