<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
        'name',
        'description'
    ];

    public function getTeamLeader()
    {
        $users = User::all()->where('role', '=', 'leader');
        foreach($users as $user){
            if($this->id === $user->team_id)
                return $user;
        }
        return false;
    }

    public function hasUser($user)
    {
        if($this->id === $user->team_id)
            return true;
        return false;
    }

    /**
     * Relationship Methods
     *
     */
    public function users(){
        return $this->hasMany(User::class);
    }
}
