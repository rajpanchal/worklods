<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class Task extends Model
{
    protected $fillable = [
        'title',
        'description',
        'due_date',
        'priority',
        'status',
        'user_id',
        'team_id',
    ];
    public function getUser($user_id) {
        return DB::table('users')->where('id', $user_id)->get()->first();
    }
    public static function autoAssign($team_id) {
        $totalPoints = 0;
        $allUsersTotalPoint = [];
        $users = User::all()->where('team_id', '=', $team_id);
        foreach($users as $user) {
            $points = Points::all()->where('user_id', '=', $user->id);
            // dd($points->first() == null);
            if($points->first() != null){
                foreach($points as $point) {
                    $totalPoints += $point->point;
                }
                array_push($allUsersTotalPoint, [['user_id' => $user->id], ['point' => $totalPoints]]);
                // dd($allUsersTotalPoint);
                $totalPoints = 0;
            }
        }
        // dd($allUsersTotalPoint);
        $bestUser = collect($allUsersTotalPoint)->sortBy($totalPoints);
        // dd($bestUser->first()[0]['user_id']);
        return $bestUser->first()[0]['user_id'];
    }

    /**
     * Relationship Methods
     */
    public function owner(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
