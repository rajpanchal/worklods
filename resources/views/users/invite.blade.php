@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <form>
                        <div class="form-group">
                            <h4>Invite Users</h4>
                            <p class="form-text text-muted">Invite your team and start creating great things together.</p>
                            <input type="text" class="form-control mt-2" id="member1" name="member1" placeholder="Email address">
                            <input type="text" class="form-control mt-2" id="member2" name="member2" placeholder="Email address">
                            <input type="text" class="form-control mt-2" id="member3" name="member3" placeholder="Email address">
                        </div>
                        @if (Gate::allows('isAdmin'))
                            <button type="button" class="btn btn-primary">Invite Team</button>
                        @elseif (Gate::allows('isMember'))
                            <button type="button" class="btn btn-primary">Join Team</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
