@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-10">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('tasks.store') }}" method="POST">
                        @csrf
                        @if (Gate::allows('isLeader'))
                            <input type="text" name="team_id" id="team_id" value={{ auth()->user()->team_id }}>
                            <div class="form-group">
                                <h4>Create Tasks</h4>
                                <label for="title" class="mt-2">Title</label>
                                <input type="text"
                                       value="{{ old('title') }}"
                                       class="form-control @error('title') is-invalid @enderror"
                                       id="title" name="title"
                                       placeholder="Task Title">
                                @error('title')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="description">Description</label>
                                <input id="description" type="hidden" name="description" value="{{ old('description') }}">
                                <trix-editor input="description"></trix-editor>
                                @error('description')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="user" class="mt-2">Assign To</label>
                                <select name="user" id="user" class="form-control">
                                    <option value="-2" >Automatic</option>
                                    <option value="-1" >Unassigned</option>
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}" >{{ $user->name }}</option>
                                    @endforeach
                                </select>
                                @error('user')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="due_date">Due Date</label>
                                <input type="date"
                                       value="{{ old('due_date') }}"
                                       class="form-control"
                                       name="due_date" id="due_date"
                                       min={{ Carbon\Carbon::now() }}>
                            </div>

                            <div class="form-group">
                                <label for="priority" class="mt-2">Priority</label>
                                <select name="priority" id="priority" class="form-control">
                                    <option value="2">Low</option>
                                    <option value="5">Medium</option>
                                    <option value="8">High</option>
                                </select>
                                @error('priority')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <button class="btn btn-success mt-4" type="submit">Add Task</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.css">
@endsection
@section('page-level-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.js"></script>
@endsection


