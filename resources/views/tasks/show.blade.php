@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header bg-dark text-white">
                    Task Details
                    <a class="back float-right" href="{{ route('tasks.index') }}"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Back</a>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-9">
                            <h4 class="mb-4">{{ $task->title }}</h4>
                            <small class="font-weight-bold">Description</small>
                            <p class="mt-2">{!! $task->description !!}</p>
                            @if (Gate::allows('isLeader'))
                                <form action="{{ route('tasks.edit', $task->id) }}" method="GET">
                                    @csrf
                                    <button class="btn btn-dark" type="submit">Edit Task</button>
                                </form>
                            @endif
                        </div>

                        <div class="col-md-3">
                            <div>
                                <small class="font-weight-bold d-block">Status</small>
                                @if ($task->status == "open")
                                <p class="d-inline-block mt-2 pl-2 pr-2 bg-secondary text-white">Open</p>
                                @elseif ($task->status == "inprogress")
                                    <p class="d-inline-block mt-2 pl-2 pr-2 bg-primary text-white">In Progress</p>
                                    @if (Gate::allows('isMember'))
                                        <div class="d-flex">
                                            <form action="{{ route('tasks.update', $task->id) }}" method="POST">
                                                @csrf
                                                @method('PUT')
                                                <input type="hidden" id="status" name="status" value="quitted">
                                                <button class="btn btn-danger btn-sm mr-2" type="submit">Quit Task</button>
                                            </form>
                                            <form action="{{ route('tasks.update', $task->id) }}" method="POST">
                                                @csrf
                                                @method('PUT')
                                                <input type="hidden" id="status" name="status" value="underreview">
                                                <button class="btn btn-success btn-sm" type="submit">Submit Task</button>
                                            </form>
                                        </div>
                                    @endif
                                @elseif($task->status == "quitted")
                                    <p class="d-inline-block mt-2 pl-2 pr-2 bg-danger text-white">Quitted</p>
                                    @if (Gate::allows('isLeader'))
                                        <div class="d-flex">
                                            <form action="{{ route('tasks.edit', $task->id) }}" method="GET">
                                                @csrf

                                                <input type="hidden" id="status" name="status" value="inprogress">
                                                <button class="btn btn-secondary btn-sm mr-2" type="submit">Reassign</button>
                                            </form>
                                        </div>
                                    @endif
                                @elseif($task->status == "underreview")
                                    <p class="d-inline-block mt-2 pl-2 pr-2 bg-warning">Under Review</p>
                                    @if (Gate::allows('isLeader'))
                                        <div class="d-flex">
                                            <form action="{{ route('tasks.update', $task->id) }}" method="POST">
                                                @csrf
                                                @method('PUT')
                                                <input type="hidden" id="status" name="status" value="rejected">
                                                <button class="btn btn-danger btn-sm mr-2" type="submit">Reject</button>
                                            </form>
                                            <form action="{{ route('tasks.update', $task->id) }}" method="POST">
                                                @csrf
                                                @method('PUT')
                                                <input type="hidden" id="status" name="status" value="approved">
                                                <button class="btn btn-success btn-sm" type="submit">Approve</button>
                                            </form>
                                        </div>
                                    @endif
                                @elseif($task->status == "rejected")
                                    <p class="d-inline-block mt-2 pl-2 pr-2 bg-danger text-white">Rejected</p>
                                @elseif($task->status == "approved")
                                    <p class="d-inline-block mt-2 pl-2 pr-2 bg-success text-white">Approved</p>
                                @endif
                            </div>

                            <div class="mt-2">
                                <small class="font-weight-bold">Assignee</small>
                                @if ($task->owner)
                                    <div class="mt-2">
                                        <img src="{{ $task->owner->avatar }}" alt=""> {{ $task->owner->name }}
                                    </div>
                                @else
                                    <p class="text-muted mt-2">Not Yet Assigned</p>
                                @endif
                            </div>
                            <div class="mt-4">
                                <small class="font-weight-bold">Due Date</small>
                                <p class="mt-2">{{ date('d/m/Y', strtotime($task->due_date)) }}</p>
                            </div>
                            <div class="mt-4">
                                <small class="font-weight-bold">Priority</small>
                                <p class="mt-2">
                                    @if ($task->priority == 2)
                                        <i class="fa fa-long-arrow-down low" aria-hidden="true"></i> Low
                                    @elseif ($task->priority == 5)
                                        <i class="fa fa-long-arrow-up medium" aria-hidden="true"></i> Medium
                                    @elseif ($task->priority == 8)
                                        <i class="fa fa-long-arrow-up high" aria-hidden="true"></i> High
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css">
@endsection
@section('page-level-scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>
@endsection

