@extends('layouts.app')

@section('content')
<div class="container">
<form>
    <div class="form-group">
        <h2 class="d-inline-block">Tasks</h2>
        @if (Gate::allows('isLeader'))
            <a href="{{ route('tasks.create') }}" class="btn btn-success float-right">Create New Task</a>
        @endif

        <ul class="nav nav-tabs mt-4" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" id="open-tab" data-toggle="tab" href="#open" role="tab" aria-controls="open" aria-selected="true">Open</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="inprogress-tab" data-toggle="tab" href="#inprogress" role="tab" aria-controls="inprogress" aria-selected="false">In Progress</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="quitted-tab" data-toggle="tab" href="#quitted" role="tab" aria-controls="quitted" aria-selected="false">Quitted</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="underreview-tab" data-toggle="tab" href="#underreview" role="tab" aria-controls="underreview" aria-selected="false">Under Review</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="rejected-tab" data-toggle="tab" href="#rejected" role="tab" aria-controls="rejected" aria-selected="false">Rejected</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="approved-tab" data-toggle="tab" href="#approved" role="tab" aria-controls="approved" aria-selected="false">Approved</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="open" role="tabpanel" aria-labelledby="open-tab">
                <table class="table table-hover mt-4">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">Sr.No.</th>
                        <th scope="col">Task</th>
                        <th scope="col">Assignee</th>
                        <th scope="col">Details </th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($tasks as $task)
                            @if (auth()->user()->isAdmin())
                                @if ($task->status == "open")
                                    <tr>
                                        <th scope="row">{{ $task->id }}</th>
                                        <td>
                                            {{ $task->title }}
                                            <small class="d-block">{{ $task->created_at->diffForHumans() }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">Not Yet Assigned</small>
                                        </td>
                                        <td>
                                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm">View <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @elseif (auth()->user()->isLeader())
                                @if ($task->status == "open"  && $task->team_id == auth()->user()->team_id)
                                    <tr>
                                        <th scope="row">{{ $task->id }}</th>
                                        <td>
                                            {{ $task->title }}
                                            <small class="d-block">{{ $task->created_at->diffForHumans() }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">Not Yet Assigned</small>
                                        </td>
                                        <td>
                                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm">View <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @elseif (auth()->user()->isMember())
                                @if ($task->status == "open"  && $task->team_id == auth()->user()->team_id && $task->user_id == auth()->user()->id)
                                    <tr>
                                        <th scope="row">{{ $task->id }}</th>
                                        <td>
                                            {{ $task->title }}
                                            <small class="d-block">{{ $task->created_at->diffForHumans() }}</small>
                                        </td>
                                        <td>
                                            <small class="text-muted">Not Yet Assigned</small>
                                        </td>
                                        <td>
                                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm">View <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="inprogress" role="tabpanel" aria-labelledby="inprogress-tab">
                <table class="table table-hover mt-4">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">Sr.No.</th>
                        <th scope="col">Task</th>
                        <th scope="col">Assignee</th>
                        <th scope="col">Details </th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($tasks as $task)
                            @if (auth()->user()->isAdmin())
                                @if ($task->status == "inprogress")
                                    <tr>
                                        <th scope="row">{{ $task->id }}</th>
                                        <td>{{ $task->title }}</td>
                                        <td><img src="{{ $task->owner->avatar }}" title="{{ $task->getUser($task->user_id)->name }}"></td>
                                        <td>
                                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm">View <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @elseif (auth()->user()->isLeader())
                                @if ($task->status == "inprogress" && $task->team_id == auth()->user()->team_id)
                                    <tr>
                                        <th scope="row">{{ $task->id }}</th>
                                        <td>{{ $task->title }}</td>
                                        <td><img src="{{ $task->owner->avatar }}" title="{{ $task->getUser($task->user_id)->name }}"></td>
                                        <td>
                                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm">View <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @elseif (auth()->user()->isMember())
                                @if ($task->status == "inprogress" && $task->team_id == auth()->user()->team_id && $task->user_id == auth()->user()->id)
                                    <tr>
                                        <th scope="row">{{ $task->id }}</th>
                                        <td>{{ $task->title }}</td>
                                        <td><img src="{{ $task->owner->avatar }}" title="{{ $task->getUser($task->user_id)->name }}"></td>
                                        <td>
                                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm">View <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="quitted" role="tabpanel" aria-labelledby="quitted-tab">
                <table class="table table-hover mt-4">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">Sr.No.</th>
                        <th scope="col">Task</th>
                        <th scope="col">Assignee</th>
                        <th scope="col">Details </th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($tasks as $task)
                            @if (auth()->user()->isAdmin())
                                @if ($task->status == "quitted")
                                    <tr>
                                        <th scope="row">{{ $task->id }}</th>
                                        <td>{{ $task->title }}</td>
                                        <td><img src="{{ $task->owner->avatar }}" title="{{ $task->getUser($task->user_id)->name }}"></td>
                                        <td>
                                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm">View <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @elseif (auth()->user()->isLeader())
                                @if ($task->status == "quitted" && $task->team_id == auth()->user()->team_id)
                                    <tr>
                                        <th scope="row">{{ $task->id }}</th>
                                        <td>{{ $task->title }}</td>
                                        <td><img src="{{ $task->owner->avatar }}" title="{{ $task->getUser($task->user_id)->name }}"></td>
                                        <td>
                                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm">View <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @elseif (auth()->user()->isMember())
                                @if ($task->status == "quitted" && $task->team_id == auth()->user()->team_id && $task->user_id == auth()->user()->id)
                                    <tr>
                                        <th scope="row">{{ $task->id }}</th>
                                        <td>{{ $task->title }}</td>
                                        <td><img src="{{ $task->owner->avatar }}" title="{{ $task->getUser($task->user_id)->name }}"></td>
                                        <td>
                                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm">View <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="underreview" role="tabpanel" aria-labelledby="underreview-tab">
                <table class="table table-hover mt-4">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">Sr.No.</th>
                        <th scope="col">Task</th>
                        <th scope="col">Assignee</th>
                        <th scope="col">Details </th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($tasks as $task)
                            @if (auth()->user()->isAdmin())
                                @if ($task->status == "underreview")
                                    <tr>
                                        <th scope="row">{{ $task->id }}</th>
                                        <td>{{ $task->title }}</td>
                                        <td><img src="{{ $task->owner->avatar }}" title="{{ $task->getUser($task->user_id)->name }}"></td>
                                        <td>
                                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm">View <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @elseif (auth()->user()->isLeader())
                                @if ($task->status == "underreview" && $task->team_id == auth()->user()->team_id)
                                    <tr>
                                        <th scope="row">{{ $task->id }}</th>
                                        <td>{{ $task->title }}</td>
                                        <td><img src="{{ $task->owner->avatar }}" title="{{ $task->getUser($task->user_id)->name }}"></td>
                                        <td>
                                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm">View <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @elseif (auth()->user()->isMember())
                                @if ($task->status == "underreview" && $task->team_id == auth()->user()->team_id && $task->user_id == auth()->user()->id)
                                    <tr>
                                        <th scope="row">{{ $task->id }}</th>
                                        <td>{{ $task->title }}</td>
                                        <td><img src="{{ $task->owner->avatar }}" title="{{ $task->getUser($task->user_id)->name }}"></td>
                                        <td>
                                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm">View <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="rejected" role="tabpanel" aria-labelledby="rejected-tab">
                <table class="table table-hover mt-4">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">Sr.No.</th>
                        <th scope="col">Task</th>
                        <th scope="col">Assignee</th>
                        <th scope="col">Details </th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($tasks as $task)
                            @if (auth()->user()->isAdmin())
                                @if ($task->status == "rejected")
                                    <tr>
                                        <th scope="row">{{ $task->id }}</th>
                                        <td>{{ $task->title }}</td>
                                        <td><img src="{{ $task->owner->avatar }}" title="{{ $task->getUser($task->user_id)->name }}"></td>
                                        <td>
                                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm">View <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @elseif (auth()->user()->isLeader())
                                @if ($task->status == "rejected" && $task->team_id == auth()->user()->team_id)
                                    <tr>
                                        <th scope="row">{{ $task->id }}</th>
                                        <td>{{ $task->title }}</td>
                                        <td><img src="{{ $task->owner->avatar }}" title="{{ $task->getUser($task->user_id)->name }}"></td>
                                        <td>
                                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm">View <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @elseif (auth()->user()->isMember())
                                @if ($task->status == "rejected" && $task->team_id == auth()->user()->team_id && $task->user_id == auth()->user()->id)
                                    <tr>
                                        <th scope="row">{{ $task->id }}</th>
                                        <td>{{ $task->title }}</td>
                                        <td><img src="{{ $task->owner->avatar }}" title="{{ $task->getUser($task->user_id)->name }}"></td>
                                        <td>
                                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm">View <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="approved" role="tabpanel" aria-labelledby="approved-tab">
                <table class="table table-hover mt-4">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">Sr.No.</th>
                        <th scope="col">Task</th>
                        <th scope="col">Assignee</th>
                        <th scope="col">Details </th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($tasks as $task)
                            @if (auth()->user()->isAdmin())
                                @if ($task->status == "approved")
                                    <tr>
                                        <th scope="row">{{ $task->id }}</th>
                                        <td>{{ $task->title }}</td>
                                        <td><img src="{{ $task->owner->avatar }}" title="{{ $task->getUser($task->user_id)->name }}"></td>
                                        <td>
                                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm">View <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @elseif (auth()->user()->isLeader())
                                @if ($task->status == "approved" && $task->team_id == auth()->user()->team_id)
                                    <tr>
                                        <th scope="row">{{ $task->id }}</th>
                                        <td>{{ $task->title }}</td>
                                        <td><img src="{{ $task->owner->avatar }}" title="{{ $task->getUser($task->user_id)->name }}"></td>
                                        <td>
                                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm">View <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @elseif (auth()->user()->isMember())
                                @if ($task->status == "approved" && $task->team_id == auth()->user()->team_id && $task->user_id == auth()->user()->id)
                                    <tr>
                                        <th scope="row">{{ $task->id }}</th>
                                        <td>{{ $task->title }}</td>
                                        <td><img src="{{ $task->owner->avatar }}" title="{{ $task->getUser($task->user_id)->name }}"></td>
                                        <td>
                                            <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm">View <i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                @endif
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</form>
</div>

@endsection

