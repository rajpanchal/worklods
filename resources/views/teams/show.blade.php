@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header bg-dark text-white">
                    Team Details
                    <a class="back float-right" href="{{ route('teams.index') }}"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Back</a>
                </div>
                <div class="card-body">

                    <h4>{{ $team->name }} Team</h4>
                    <p>{{ $team->description }}</p>
                    <form action="{{ route('teams.edit', $team->id) }}" method="GET">
                        @csrf
                        @if (Gate::allows('isAdmin'))
                            <button class="btn btn-dark float-right" type="submit">Edit Team</button>
                        @endif
                    </form>

                    <h5 class="mt-4">Members ({{$users->count()}})</h5>
                    <div class="row mt-4">
                        <div class="col-4">
                            <div class="list-group" id="list-tab" role="tablist">
                                @foreach ($users as $user)
                                    <a class="list-group-item list-group-item-action" id="list-{{ $user->id }}-list" data-toggle="list" href="#list-{{ $user->id }}" role="tab" aria-controls="{{ $user->id }}">{{ $user->name }}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="tab-content" id="nav-tabContent">
                                @foreach ($users as $user)
                                    <div class="tab-pane fade show text-dark" id="list-{{ $user->id }}" role="tabpanel" aria-labelledby="list-{{ $user->id }}-list">
                                        <div class="card">
                                            <div class="card-body">
                                                <p>Name : {{ $user->name }}</p>
                                                <p>Role : {{ Str::ucfirst($user->role) }}</p>
                                                <p>Email : {{ $user->email }}</p>

                                                <div class="d-flex">
                                                    <form action="{{ route('users.remove', $user->id) }}" method="GET">
                                                        @csrf
                                                        @if ((Gate::allows('isLeader')) && (auth()->user()->team_id == $team->id))
                                                            <button class="btn btn-danger btn-sm mr-2" type="submit">Remove</button>
                                                        @endif
                                                    </form>

                                                    <form action="{{ route('users.makeLeader', $user->id) }}" method="GET">
                                                        @csrf
                                                        @if ((Gate::allows('isAdmin')) && ($user->role == "member"))
                                                            <button class="btn btn-primary btn-sm" type="submit">Make Leader</button>
                                                        @endif
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css">
@endsection
@section('page-level-scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>
@endsection

