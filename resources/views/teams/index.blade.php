@extends('layouts.app')

@section('content')
<div class="container">
<form>
    <div class="form-group">
        <h2 class="d-inline-block">Team</h2>
        @if (Gate::allows('isAdmin'))
            <a href="{{ route('teams.create') }}" class="btn btn-success float-right">Create New Team</a>
        @endif
        <table class="table table-hover mt-4">
            <thead class="thead-dark">
              <tr>
                <th scope="col">Sr. No.</th>
                <th scope="col">Name</th>
                <th scope="col">Leader</th>
                <th scope="col">Created At</th>
                <th scope="col">Details</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($teams as $team)
                    <tr>
                        <th scope="row">{{ $team->id }}</th>
                        <td>{{ $team->name }}</td>
                        <td>{{ ($team->getTeamLeader()) ? $team->getTeamLeader()->name : 'No Leader Assigned' }}</td>
                        <td>{{ $team->created_at }}</td>
                        <td>
                            <a href="{{ route('teams.show', $team->id) }}" class="btn btn-primary btn-sm edit">View <i class="fa fa-eye"></i></a>
                            {{-- <button type="button" class="btn btn-outline-danger btn-sm delete" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash"></i></button> --}}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</form>
</div>
{{-- <div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-10">
            <div class="card">
                <div class="card-body">

                </div>
            </div>
        </div>
    </div>
</div> --}}


<!-- Delete Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Delete Team</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('teams.destroy', $team->id) }}">
                @method('DELETE')
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="record_id" id="delete_record_id">
                    <p class="text-muted">Are you sure you want to delete team?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" name="deleteTeam">Delete Team</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /Delete Modal -->
@endsection

