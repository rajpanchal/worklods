@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('teams.update', $team->id) }}" method="POST">
                        @csrf
                        @method('PUT')

                        @if (Gate::allows('isAdmin'))
                            <div class="form-group">
                                <h4>Edit Team</h4>
                                <input type="text"
                                       value="{{ old('name', $team->name) }}"
                                       class="form-control mt-2 @error('name') is-invalid @enderror"
                                       id="name" name="name"
                                       placeholder="Team Name">
                                @error('name')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description"
                                          id="description"
                                          class="form-control @error('description') is-invalid @enderror"
                                          rows="4">{{ old('description', $team->description) }}</textarea>
                                @error('description')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="users" class="mt-2">Members</label>
                                <select name="users[]" id="users" class="form-control select2" multiple>

                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}"
                                            {{ old('users') ? (in_array($user->id, old('users')) ? 'selected': ''): ($team->hasUser($user) ? 'selected' : '') }}>{{ $user->name }}</option>
                                    @endforeach
                                </select>
                                @error('users')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <button class="btn btn-success mt-4" type="submit">Update Team</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css">
@endsection
@section('page-level-scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>
@endsection

