@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        {{-- <div class="col-md-3">
            <div class="card">
                <div class="card-header">Sidebar</div>

                <div class="card-body">
                    <ul>
                        <li>Home</li>
                    </ul>
                </div>
            </div>
        </div> --}}
        <div class="col-md-12">
            <h1>Your Work</h1>

            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body">
                            <h5>Empolyee Of The Month</h5>
                            <p>
                                @if ((date('d', strtotime(Carbon\Carbon::tomorrow()))) == "1")
                                    <p>{{ auth()->user()->bestEmpolyeeOfTheMonth() }}</p>
                                @endif
                                @if (date('d') === "08")
                                    {{-- {{ auth()->user()->bestEmpolyeeOfTheMonth()  }} --}}
                                    {{-- {{ date('d', strtotime(Carbon\Carbon::tomorrow())) }} --}}
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

